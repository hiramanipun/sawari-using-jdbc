package sawari.booking.sawari.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import sawari.booking.sawari.models.Vehicles;
import sawari.booking.sawari.service.interfaces.VehiclesInterface;

@RestController
@RequestMapping("vehicles")

public class VehicleController {
    @Autowired
    private VehiclesInterface vehiclesInterface;
    @GetMapping("vehicle/{id}")
    public Vehicles getVehicle(@PathVariable("id") long id){
     return    vehiclesInterface.vehicleById(id);
    }
    @PostMapping("save")
    public int saveVehicle(@RequestBody Vehicles vehicles){
       return  vehiclesInterface.save(new Vehicles(vehicles.getFk_location_id(),vehicles.getFk_vehicle_type(),vehicles.getFk_vehicle_model(),vehicles.getTotal_seats(),vehicles.getColor(),vehicles.getAc_noac(), vehicles.getImage(), vehicles.getGadi_no(),vehicles.getGadiko_wiwaran()));

    }
}
