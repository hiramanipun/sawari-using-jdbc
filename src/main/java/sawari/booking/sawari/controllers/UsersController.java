package sawari.booking.sawari.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;
import sawari.booking.sawari.service.interfaces.UsersDao;
import sawari.booking.sawari.models.UsersModel;

import java.util.List;

@RestController
//@RequestMapping("/apis")
public class UsersController {
    @Qualifier("userDaoImplementation")
    @Autowired
    private UsersDao usersDao;
    @GetMapping(value = "/user/{id}")
    public UsersModel getUser(@PathVariable("id") int id){
return  usersDao.userById(id);
    }
    @PostMapping(value = "create")
    public UsersModel create(@RequestBody UsersModel usersModel){
        return usersDao.addUser(new UsersModel(usersModel.getUsername(),usersModel.getPassword(),usersModel.getName(),usersModel.getPhone(),usersModel.getEmail(),usersModel.getPhoto(),usersModel.getFk_province_id(),usersModel.getFk_district_id(),usersModel.getFk_municipal_id(),usersModel.getAddress(),usersModel.getOthers()));
    }
    @GetMapping("users")
    public List<UsersModel> allUsers(){
return usersDao.getAll();
    }
    @PutMapping("update/{id}")
    public UsersModel update(@RequestBody UsersModel usersModel,@PathVariable("id") int id){
        UsersModel _user = getUser(id);
        if(_user!=null){
            _user.setUsername(usersModel.getUsername());
            _user.setPassword(usersModel.getPassword());
            _user.setId(id);
            return usersDao.update(_user);
        }else{
            return null;
        }
    }
}
