package sawari.booking.sawari.mapper;

import org.springframework.jdbc.core.RowMapper;
import sawari.booking.sawari.models.Vehicles;

import javax.swing.tree.TreePath;
import java.sql.ResultSet;
import java.sql.SQLException;

public class VehiclesDetailsMapper implements RowMapper<Vehicles> {
    public Vehicles mapRow(ResultSet resultSet, int i) throws SQLException {
        Vehicles vehicles=new Vehicles();
        vehicles.setId(resultSet.getInt("id"));
        vehicles.setLocation(resultSet.getString("location"));
        vehicles.setType(resultSet.getString("type"));
        vehicles.setModel(resultSet.getString("model"));
        vehicles.setTotal_seats(resultSet.getDouble("total_seats"));
        vehicles.setColor(resultSet.getString("color"));
        vehicles.setAc_noac(resultSet.getInt("ac_noac"));
        vehicles.setCreated_date(resultSet.getString("created_date"));
        vehicles.setOwner(resultSet.getString("owner"));
        vehicles.setImage(resultSet.getString("image"));
        vehicles.setGadi_no(resultSet.getString("gadi_no"));
        vehicles.setGadiko_wiwaran(resultSet.getString("gadiko_wiwaran"));
        return vehicles;
    }
}
