package sawari.booking.sawari.service.interfaces;

import sawari.booking.sawari.models.UsersModel;

import java.util.List;
public interface UsersDao {
    public UsersModel addUser(UsersModel usersModel);
    public UsersModel userById(int id);
    public List<UsersModel> getAll();
    public UsersModel update(UsersModel usersModel);
    public int delete(int id);
}
