package sawari.booking.sawari.service.interfaces;

import sawari.booking.sawari.models.Vehicles;

import java.util.List;

public interface VehiclesInterface {
    public int save(Vehicles vehicles);
    public Vehicles vehicleById(long id);
    public List<Vehicles> vehicles();
    public Vehicles update(Vehicles vehicles);
    public int delete();
}
