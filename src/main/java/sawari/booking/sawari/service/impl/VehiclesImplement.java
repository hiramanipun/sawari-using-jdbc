package sawari.booking.sawari.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.tags.EditorAwareTag;
import sawari.booking.sawari.config.JdbcConfiguration;
import sawari.booking.sawari.mapper.VehiclesDetailsMapper;
import sawari.booking.sawari.models.Vehicles;
import sawari.booking.sawari.service.interfaces.VehiclesInterface;

import javax.sql.DataSource;
import java.time.LocalDate;
import java.util.List;
@Service
@Slf4j
public class VehiclesImplement implements VehiclesInterface {
    @Autowired
  private  JdbcConfiguration jdbcConfiguration;
  private JdbcTemplate jdbcTemplate;
  public void setJdbcTemplate(DataSource dataSource){
      this.jdbcTemplate = new JdbcTemplate(dataSource);
  }
    @Override
    public int save(Vehicles vehicles) {
        setJdbcTemplate(jdbcConfiguration.dataSource());
        LocalDate date = LocalDate.now();
       // GeneratedKeyHolder generatedKeyHolder = new GeneratedKeyHolder();
        final String sql = "INSERT INTO sawari_vehicle_details(fk_location_id,fk_vehicle_type,fk_vehicle_model,total_seats,color,ac_noac,status_num,created_date,fk_user_id,fk_owner_id,status_char,image,gadi_no,gadiko_wiwaran) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        return jdbcTemplate.update(sql,new Object[]{vehicles.getFk_location_id(),vehicles.getFk_vehicle_type(),vehicles.getFk_vehicle_model(),vehicles.getTotal_seats(),vehicles.getColor(),vehicles.getAc_noac(),vehicles.AVAILABLE_NUM,date,1,vehicles.getFk_owner_id(),vehicles.AVAILABLE,vehicles.getImage(),vehicles.getGadi_no(),vehicles.getGadiko_wiwaran()});
      //  return vehicleById(generatedKeyHolder.getKey().longValue());
    }

    @Override
    public Vehicles vehicleById(long id) {
        setJdbcTemplate(jdbcConfiguration.dataSource());
        final String sql = "SELECT sawari_vehicle_details.id,sawari_location.location,sawari_vehicle_type.type,sawari_vehicle_model.model,sawari_vehicle_details.total_seats,sawari_vehicle_details.color,sawari_vehicle_details.ac_noac,sawari_vehicle_details.created_date,sawari_owner.name as owner,sawari_vehicle_details.image,sawari_vehicle_details.gadi_no,sawari_vehicle_details.gadiko_wiwaran FROM sawari_vehicle_details LEFT JOIN sawari_location ON sawari_location.id=sawari_vehicle_details.fk_location_id " +
                " LEFT JOIN  sawari_vehicle_type ON sawari_vehicle_type.id=sawari_vehicle_details.fk_vehicle_type" +
                " LEFT JOIN sawari_owner ON sawari_owner.id=sawari_vehicle_details.fk_owner_id " +
                " LEFT JOIN sawari_vehicle_model ON sawari_vehicle_model.id=sawari_vehicle_details.fk_vehicle_model " +
                " WHERE sawari_vehicle_details.id=?";
        return jdbcTemplate.queryForObject(sql, new VehiclesDetailsMapper(),id);
    }

    @Override
    public List<Vehicles> vehicles() {
        return null;
    }

    @Override
    public Vehicles update(Vehicles vehicles) {
        return null;
    }

    @Override
    public int delete() {
        return 0;
    }
}
