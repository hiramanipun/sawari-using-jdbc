package sawari.booking.sawari.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import sawari.booking.sawari.config.JdbcConfiguration;
import sawari.booking.sawari.models.UsersModel;
import sawari.booking.sawari.service.interfaces.UsersDao;

import javax.sql.DataSource;
import java.time.LocalDate;
import java.util.List;
import java.util.Random;

@Service
@Slf4j
@Qualifier("userDaoImplementation")
public class UserDaoImplementation implements UsersDao {

    private JdbcTemplate jdbcTemplate;
    @Autowired
    private JdbcConfiguration jdbcConfiguration;
    public void setJdbcTemplate(DataSource dataSource){
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public UsersModel addUser(UsersModel usersModel) {
        setJdbcTemplate(jdbcConfiguration.dataSource());
        String key = generateRandomPassword(8);
        LocalDate date = LocalDate.now();
        int flag = jdbcTemplate.update("INSERT INTO users(username,password,auth_key,name,phone,email,photo,fk_province_id,fk_district_id,fk_municipal_id,address,others,user_type,created_at,updated_at,fk_user_id,fk_service_place,access_token,firebase_token,sawari_owner_id,status) VALUES " +
                "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",new Object[]{usersModel.getUsername(),usersModel.getPassword(),key,usersModel.getName(),usersModel.getPhone(),usersModel.getEmail(),usersModel.getPhoto(),usersModel.getFk_province_id(),usersModel.getFk_district_id(),usersModel.getFk_municipal_id(),usersModel.getAddress(),usersModel.getOthers(),1,date,date,1,1,key,key,1,1});
        if(flag==1){
            return this.userById(usersModel.getId());
        }else{
            return null;
        }
    }
    public static String generateRandomPassword(int len) {
        String chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijk"
                +"lmnopqrstuvwxyz!@#$%&";
        Random rnd = new Random();
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++)
            sb.append(chars.charAt(rnd.nextInt(chars.length())));
        return sb.toString();
    }

    @Override
    public UsersModel userById(int id) {
        setJdbcTemplate(jdbcConfiguration.dataSource());
        try{
            UsersModel usersModel= jdbcTemplate.queryForObject("SELECT * FROM users WHERE id=?", BeanPropertyRowMapper.newInstance(UsersModel.class),id);
            return usersModel;
        }catch (IncorrectResultSizeDataAccessException e){
            return null;
        }

    }


    @Override
    public List<UsersModel> getAll() {
        setJdbcTemplate(jdbcConfiguration.dataSource());
        return jdbcTemplate.query("SELECT * FROM users",BeanPropertyRowMapper.newInstance(UsersModel.class));
    }

    @Override
    public UsersModel update(UsersModel usersModel) {
        setJdbcTemplate(jdbcConfiguration.dataSource());
      int result =  jdbcTemplate.update("UPDATE users SET username = ? ,password = ? WHERE id=?",usersModel.getUsername(),usersModel.getPassword(),usersModel.getId());
      if(result==1){
          return this.userById(usersModel.getId());
      }else {
          return null;
      }
    }

    @Override
    public int delete(int id) {
        return 0;
    }
}
