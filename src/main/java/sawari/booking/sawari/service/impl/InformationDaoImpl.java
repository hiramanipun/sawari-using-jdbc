package sawari.booking.sawari.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import sawari.booking.sawari.models.UsersModel;
import sawari.booking.sawari.service.interfaces.UsersDao;

import java.util.List;
@Service
@Slf4j
@Qualifier("informationDaoImpl")
public class InformationDaoImpl implements UsersDao {
    @Override
    public UsersModel addUser(UsersModel usersModel) {
        return null;
    }

    @Override
    public UsersModel userById(int id) {
        return null;
    }

    @Override
    public List<UsersModel> getAll() {
        return null;
    }

    @Override
    public UsersModel update(UsersModel usersModel) {
        return null;
    }

    @Override
    public int delete(int id) {
        return 0;
    }
}
