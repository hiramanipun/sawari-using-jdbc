package sawari.booking.sawari.models;

import lombok.*;

import java.sql.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UsersModel {
    private int id;
    private String username;
    private String password;
    private String auth_key;
    private String name;
    private String phone;
    private String email;
    private String photo;
    private int fk_province_id;
    private int fk_district_id;
    private int fk_municipal_id;
    private String address;
    private String others;
    private int user_type;
    private String created_at;
    private String updated_at;
    private int fk_user_id;
    private int fk_service_place;
    private String access_token;
    private String firebase_token;
    private int sawari_owner_id;


    public UsersModel(String username, String password, String name, String phone, String email, String photo, int fkProvinceId, int fkDistrictId, int fkMunicipalD, String address, String others) {
        this.username = username;
        this.password = password;
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.photo = photo;
        this.fk_province_id = fkProvinceId;
        this.fk_district_id =fkDistrictId;
        this.fk_municipal_id = fkMunicipalD;
        this.address = address;
        this.others = others;
    }

}
