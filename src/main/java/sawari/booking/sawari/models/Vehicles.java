package sawari.booking.sawari.models;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Vehicles {
    public final  String AVAILABLE = "available";
    public final String UNAVAILABLE = "unavailable";
    public final  int AVAILABLE_NUM = 1;
    public final  int UNAVAILABLE_NUM = 0;
    private long id;
    private long fk_location_id;
    private String location;
    private String type;
    private String model;
    private String owner;
    private long fk_vehicle_type;
    private long fk_vehicle_model;
    private double total_seats;
    private String color;
    private int ac_noac;
    private int status_num;
    private String created_date;
    private long fk_user_id;
    private long fk_owner_id;
    private String status_char;
    private String image;
    private String gadi_no;
    private String gadiko_wiwaran;

    public Vehicles(long fk_location_id, long fk_vehicle_type,long fk_vehicle_model, double total_seats, String color, int ac_noac, String image, String gadi_no, String gadiko_wiwaran) {
        this.fk_location_id = fk_location_id;
        this.fk_vehicle_model = fk_vehicle_model;
        this.total_seats = total_seats;
        this.color = color;
        this.ac_noac = ac_noac;
        this.image = image;
        this.gadi_no = gadi_no;
        this.gadiko_wiwaran = gadiko_wiwaran;
        this.fk_vehicle_type = fk_vehicle_type;
    }
}
